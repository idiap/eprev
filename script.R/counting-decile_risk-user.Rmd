### Comptatges d'usuaris i controls segons decils de risc

```{r}
source('/home/idiap/lib/R/usrgi_lib/main.R')
data$risc10 = xquantile(data$regicor, K=10)
tab<-t(table(data$user, data$risc10, useNA='ifany'))
tab
addmargins( 100*prop.table(tab, 1), 1)
addmargins( 100*prop.table(tab, 2), 2)
```


