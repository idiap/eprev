#last-generation: eprev-01 eprev-02
#	date > last-generation

all: eprev-01.info eprev-02.info

eprev-01.info: script.R/*.R
	cd eprev-01 && $(MAKE)
	date > eprev-01.info 
eprev-02.info: script.R/*.R
	cd eprev-02 && $(MAKE)
	date > eprev-02.info

www:
	rsync -avzL --delete www/ -e ssh puntdtro@puntdtrobada.com:~/public_html/mcomas/eprev/
